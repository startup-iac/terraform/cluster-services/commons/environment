locals {
  namespace = "${var.environment_name}-ns"
}

resource "kubernetes_namespace" "this" {
  metadata {
    name = local.namespace
  }
}